.. inverter documentation master file.
   You can adapt this file completely to your liking, 
   but it should at least contain the root `toctree` directive.

Exercise manager
================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: exercise_manager
   :members:
   :special-members: __init__
   :undoc-members:

.. automodule:: exercise_manager.exercise
   :members:
   :special-members: __init__
   :undoc-members:

