.. rtl documentation master file, created by
   sphinx-quickstart on Thu Aug 15 11:47:06 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================
Exercise manager
================

These pages are the documentation of Exercise maneger project, 
`https://gitlab.com/coursegit/exercise_manager <https://gitlab.com/coursegit/exercise_manager>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   sections

   indices_and_tables

