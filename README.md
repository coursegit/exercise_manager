Exercise manager
----------------

The goal of the exercise manager is to provide structured helper 
methodology to create exercises 

* For a git project to teach how to use the project
* Deliver exercises as git issues with a due date

For documentation, see 
[https://coursegit.gitlab.io/exercise_manager](https://coursegit.gitlab.io/exercise_manager)

Marko Kosunen, marko.kosunen@aalto.fi 26.8.2022


