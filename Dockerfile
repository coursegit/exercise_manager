FROM fedora:36
# Needed for mosaic_BAG documentation CI builds
RUN dnf -y install tcsh make git python3 python3-pip python3-sphinx
RUN for module in "PyYaml  numpy  scipy  h5py  zmq  shapely  rtree  future  jinja2  IPython  openmdao  sphinx_rtd_theme  python-gitlab  "; do pip3 install --user $module; done
RUN export PATH=${PATH}/${HOME}/.local:${PATH}/${HOME}/.local/bin


