'''
Exercise module
===============

This module defines a class describing a single exercise.

Initially written by Marko Kosunen, 10.9.2022, marko.kosunen@aalto.fi
'''
import os
import re
import pdb

class exercise():
    def __init__(self,**kwargs):
        '''
        Class describing course exercises

        Attributes:
        -----------
        project : str
            Project name to which the issues are attached.
        due_date : str
            Deadline for the exercise in YYYY-MM-DD format. Default '2020-01-01'.
        file : str
            Path to description file. Contains the issue body for the exercise. 
        project_template : str 
            URL of the template project
        project_template_branch : str
            Branch of the template to be used. Default 'master'.
        project_target_branch : str
            Branch of the target to push. Default 'master'.
        project_description : str
            Description string of the project.
        substitution list : [ (str,str) ]
            List of tuples that will be substituted in the description

        '''
        self.due_date=kwargs.get('due_date', None)
        self.project=kwargs.get('project',None)
        self.file=os.path.expanduser(kwargs.get('file',None)) # This makes it possible to tilde

        #These are the properties of the project_manager_projects
        self.project_name=self.project
        if not hasattr(self,'project_name'):
            sys.exit("Error: Project name must be given")
        self.project_template=kwargs.get('project_template',None)
        self.project_template_branch=kwargs.get('project_template_branch','master')
        self.project_target_branch=kwargs.get('project_target_branch','master')
        self.project_description=kwargs.get('project_description')
        self.substitution_list=kwargs.get('substitution_list',[])


    @property
    def description(self):
        if not hasattr(self,'_description'):
            if self.file != '':
                contents=''
                try:
                    with open(self.file, 'r') as f:
                        for line in f:
                            contents+=line
                    self._description=contents
                except FileNotFoundError:
                    print("File %s didn't exist, use empty value for description!" % self.description_file) 
            else:
                print("Description file path not given, using empty value for description!")
            print('Postprocessing description')
            for orig,repl in self.substitution_list:
                print('Substituting %s with %s in description' %(orig,repl))
                self._description=re.sub(r'%s'%(orig),"%s" %(repl),self._description)
        return self._description
    
    @property
    def title(self):
       '''Title   is extracted from the first line of the issue file.
       It is assumed to be a markdown header, so #'s are stripped
          
       '''
       if not hasattr(self,'_title'):
           if self.file != '':
               try:
                   with open(self.file, 'r') as f:
                       title=f.readline().strip()
               except:
                    print("ERROR: Exercise title can not be extracted.") 
           self._title=re.sub(r'^[# ]*',"",title)
           for orig,repl in self.substitution_list:
               print('Substituting %s with %s in title' %(orig,repl))
               self._title=re.sub(r'%s'%(orig),"%s" %(repl),self._title)
       return self._title

     
    @property
    def assigned(self):
       '''Dict of persons to whom this exercise has been assigned, the issue status and the issue id and the project id the issue is assigned to.
          
       '''
       if not hasattr(self,'_assigned'):
           self._assigned = dict()
       return self._assigned

    @assigned.setter
    def assigned(self,value):
       '''Dict of persons to whom this exercise has been assigned, the issue status and the issue id
          
       '''
       self._assigned = value

    @property
    def passed(self):
       '''Dict of persons to whom this exercise issue has been closed.
          
       '''
       self._passed=dict()
       for key,val in self.assigned.items():
           if val[1] == 'closed':
               self._passed[key]=val
       return self._passed

    @property
    def failed(self):
       '''Dict of persons to whom this exercise issue has been closed.
          
       '''
       self._failed=dict()
       for key,val in self.assigned.items():
           if val[1] != 'closed':
               self._failed[key]=val
       return self._failed

    def create_issue(self,**kwargs):
        proj = kwargs.get('proj',None)
        assignee_id = kwargs.get('assignee_id', None)
        if proj == None:
            print('Project for the issues must be given')
            sys.exit
        else:
            #Update existing issue in proj
            existing_issue=list(filter(
                lambda x: x.title == self.title, 
                proj.issues.list(get_all=True)
                ))
            if existing_issue:
                existing_issue=existing_issue[0]
                print('Issue with title %s already exists in project %s' 
                        %(self.title,proj.name))
                print('Updating with current data')
                existing_issue.title=self.title
                existing_issue.description=self.description
                existing_issue.due_date=self.due_date
                if assignee_id != None:
                    existing_issue.assignee_ids=[assignee_id]
                existing_issue.save()
                existing_issue = None
            else:
                issue=proj.issues.create({
                    'title' : self.title,
                    'description' : self.description,
                    'due_date'  : self.due_date
                        })
                print('Creating Issue with title %s in project %s' 
                            %(self.title,self.project_name))
                if assignee_id != None:
                    issue.assignee_ids=[assignee_id]
                issue.save()
