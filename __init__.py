"""
Gitlab exercise generator
=========================

Class for creating an 'exercise' from given Gitlab markdown
language files.

Initially written by Marko Kosunen, marko.kosunen@aalto.fi 25.8.2022
"""
import sys
import gitlab
import time
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import subprocess
import re
from gitlab.exceptions import GitlabCreateError, GitlabGetError, GitlabHttpError, GitlabDeleteError
from exercise_manager.exercise import exercise
import pdb

class exercise_manager():
    """ Exercise in a collection of issues with due dates assigned to a assignee

    Structure notes
    ---------------
       * Exercise hosted in a group, id known
       * Exercise hosting group can be thought as a 'Project', see project manager.
       * May or may not be implemented in subgroups that are to be created
       * May be adssigned on multiple persons on single project in master group, or assigned on single person in
         projects under subgroups. Not assigned if assignee_ids list is empty.
    """
    def __init__(self,**kwargs):
        self._url=kwargs.get('url', None)
        self._token=kwargs.get('token')
        self._ssl=kwargs.get('ssl')
        self._mastergroupname=kwargs.get('group', None)
        # [TODO] : handle errors.
        self.gl = gitlab.Gitlab(url=self._url,
            private_token=self._token,
            api_version=4,
            ssl_verify=self._ssl
            )
        self._subgroups_created = False

    @property
    def assign(self):
        if not hasattr(self,'_assign'):
            self._assign = False
        return self._assign
    @assign.setter
    def assign(self,val):
        self._assign = val

    @property
    def mastergroup(self):
        if not hasattr(self,'_mastergroup'):
            self._mastergroup=self.gl.groups.get(self._mastergroupname)
        return self._mastergroup

    @property
    def protected_subgroups(self):
        ''' List : str
        List of protected subgroups

        '''
        if not hasattr(self,'_protected_subgroups'):
            self._protected_subgroups=[]
        return self._protected_subgroups
    @protected_subgroups.setter
    def protected_subgroups(self,val):
        ''' List : str
        List of protected subgroups

        '''
        self._protected_subgroups=val

    @property
    def assignee_ids(self):
        '''List of is's of the persons to whow the exercise issues should be assigned to.
        If the the exercises are to be done in subgroups, the length of this list must equal the
        length of the exercise_subgroups. If exercise_subgroups is empty, exercises are creted on one
        project for all assignees.

        '''
        if not hasattr(self,'_assignee_ids'):
            self._assignee_ids=[]
        return self._assignee_ids
    @assignee_ids.setter
    def assignee_ids(self,val):
            self._assignee_ids=val

    ### Subgroup handling ###
    @property
    def exercise_subgroups(self):
        ''' List : str
        List of exercise subgroups

        '''
        if not hasattr(self,'_exercise_subgroups'):
            self._exercise_subgroups=[]
        return self._exercise_subgroups

    @exercise_subgroups.setter
    def exercise_subgroups(self,val):
        ''' List : str
        List of protected subgroups

        '''
        self._exercise_subgroups=val

    @property
    def exercise_subgroup_visibility(self):
        ''' List : public | internal | private (default)
        Visibility of the exercise subgroup, if created.

        '''
        if not hasattr(self,'_exercise_subgroup_visibility'):
            self._exercise_subgroup_visibility='private'
        return self._exercise_subgroup_visibility

    @exercise_subgroup_visibility.setter
    def exercise_subgroup_visibility(self,val):
        self._exercise_subgroup_visibility=val


    def add_subgroups(self):
        ''' Create subgroup if needed

        '''
        if len(self.exercise_subgroups) > 0 and (len(self.exercise_subgroups) == len(self.assignee_ids)):
            for subgroup in self.exercise_subgroups:
                try:
                    groupname=subgroup.replace(' ',"_")
                    groupname=groupname.replace('Ä',"A")
                    groupname=groupname.replace('ä',"a")
                    groupname=groupname.replace('Ö',"O")
                    groupname=groupname.replace('ö',"o")
                    groupname=groupname.replace('Õ',"O")
                    groupname=groupname.replace('õ',"o")
                    groupname=groupname.replace('ã',"a")
                    groupname=groupname.replace('è',"e")
                    groupname=groupname.replace('é',"e")
                    groupname=groupname.replace('ü',"u")
                    group=self.gl.groups.create({'name': subgroup, 'path': groupname, 'parent_id': self.mastergroup.id})
                    group.visibility=self.exercise_subgroup_visibility
                    group.save()
                    print('Group %s/%s created' %(self.mastergroup.name,subgroup))
                except (GitlabCreateError):
                    print('Group %s/%s already exists, not creating' %(self.mastergroup.name,subgroup))
                    group = self.mastergroup.subgroups.list(search=groupname,get_all=True)[0]
                    real_group = self.gl.groups.get(group.id)
                    if real_group.visibility != self.exercise_subgroup_visibility:
                        print('Updating visiblity of group %s to %s.' %(group.name,self.exercise_subgroup_visibility))
                        real_group.visibility=self.exercise_subgroup_visibility
                        real_group.save()
        self._subgroups_created = True

    def delete_subgroups(self,**kwargs):
        ''' Delete all subgroups if not protected

        '''
        grouplist=self.mastergroup.subgroups.list(get_all=True)
        for group in grouplist:
            if group.name not in self.protected_subgroups:
                print("Deleting %s" %(group.name))
            elif ( group.name in self.exercise_subgroups ) :
                print('%s protected , not deleting.' %(group.name))

        #Careful here!!! Everything will be deleted.
        if grouplist:
            answer=False
            while answer != 'n' and answer != 'y':
                answer=input('Is this what you want [n/y]')
            if answer == 'y':
                for group in grouplist:
                    if group.name not in self.protected_subgroups:
                        real_group = self.gl.groups.get(group.id)
                        print("Deleting %s" %(real_group.name))
                        real_group.delete()
                    else:
                        print('%s protected , not deleting.' %(group.name))
                ## Here wee need to wait the deletion to happen
                waittime=10
                print('Waiting %s seconds for server to stabilize' %(waittime))
                time.sleep(waittime)
                self._subgroups_created = False
            else:
                print('Not deleting anything')


    @property
    def subgroups(self):
        if not hasattr(self, '_subgroups'):
            if (not self._subgroups_created) and (len(self.exercise_subgroups) > 0):
                self.add_subgroups()
                self._subgroups=[]
                # We must loop this first to retain the order.
                for sg in self.exercise_subgroups:
                    # Returns all matches, also partial, must look for exact match
                    groups = self.mastergroup.subgroups.list(search=sg,get_all=True)
                    for group in groups:
                        if group.name == sg:
                            real_group = self.gl.groups.get(group.id)
                            self._subgroups.append(real_group)
            else:
                #No subgroups, Mastergroup is the subgroup
                self._subgroups=[self.mastergroup]
        return self._subgroups

    def assignees_to_subgroups(self,**kwargs):
        '''Assuming the order of the subgroups is the order of the assignee list,
        grant access to assignees to corresponding subgroup.

        Parameters
        ----------
            access_level : str
                GUEST | REPORTER | DEVELOPER | MAINTAINER | OWNER
        '''
        access_level=kwargs.get('access_level', 'MAINTAINER')
        for index in range(len(self.subgroups)):
            group=self.subgroups[index]
            assignee=self.assignee_ids[index]
            # Delete existing direct members
            for member in group.members.list():
                if member.access_level != 'OWNER' and member.id != assignee:
                    print('Deleting direct member %s' %(member.name))
                    member.delete()
            try:
                 group.members_all.get(assignee)
                 notamember = False
            except (GitlabGetError):
                 notamember = True

            if notamember:
                print("Granting access to user %s with access level %s" %(assignee,access_level))
                member = group.members.create({'user_id': assignee,
                                   'access_level': getattr(gitlab.const.AccessLevel,'%s' %(access_level))})
            else:
                try:
                    if group.members_all.list(id=assignee,get_all=True)[0].access_level < getattr(gitlab.const.AccessLevel,'%s' %(access_level)):
                        name = group.members_all.list(id=assignee,get_all=True)[0].name
                        print("User %s already member of the group, modifying access level" %(name))
                        member = group.members_all.list(id=asssignee, get_all=True)[0]
                        member.access_level=getattr(gitlab.const.AccessLevel,'%s' %(access_level))
                        member.save()
                    else:
                        name = group.members_all.list(id=assignee,get_all=True)[0].name
                        print("User %s inherited member of the group, with level higher than requested" %(name))
                except (GitlabCreateError,GitlabDeleteError):
                    print("User already member of the group, access level can not be modified")

    ### Subgroup handling ends ###
    def assignees_to_project(self,**kwargs):
        '''Assign the assignee list of users to a project

        Parameters
        ----------
            access_level : str
                GUEST | REPORTER | DEVELOPER | MAINTAINER | OWNER
            project : str
                Name or ID of the project
        '''
        access_level=kwargs.get('access_level', 'DEVELOPER')
        project_name_or_id=kwargs.get('project', None)
        project = self.gl.projects.get(project_name_or_id)
        for assignee in self.assignee_ids:
            try:
                member=project.members.get(assignee)
                notamember=False
            except (GitlabGetError):
                notamember=True

            if notamember:
                print("Granting access to user %s with access level %s" %(assignee,access_level))
                member = project.members.create({'user_id': assignee,
                                   'access_level': getattr(gitlab.const.AccessLevel,'%s' %(access_level))})
            else:
                print( 'Assignee %s already direct member of the project.' %(member.name))
            #else:
            #    print("User already member of the group, modifying access level")
            #    try:
            #         group.members_all.get(assignee)
            #         notamember = False
            #    except (GitlabGetError):
            #         notamember = True
            #    if notamember:
            #        member = project.members.create({'user_id': assignee,
            #                           'access_level': getattr(gitlab.const.AccessLevel,'%s' %(access_level))})

            #    else:
            #        print("User already member of the group, modifying access level")
            #        try:
            #            if group.members_all.list(id=assignee,get_all=True)[0].access_level < getattr(gitlab.const.AccessLevel,'%s' %(access_level)):
            #                member = group.members_all.list(id=asssignee, get_all=True)[0]
            #                member.access_level=getattr(gitlab.const.AccessLevel,'%s' %(access_level))
            #                member.save()
            #            else:
            #                print("User inherited member of the group, with level higher than requested")
            #        except (GitlabCreateError,GitlabDeleteError):
            #            print("User already member of the group, access level can not be modified")

    ### Project handling ###
    def delete_direct_members_from_project(self,**kwargs):
        project_name_or_id=kwargs.get('project', None)
        project = self.gl.projects.get(project_name_or_id)
        for assignee in project.members.list(get_all=True):
            print('Deleting direct member %s form project.' %(assignee.name))
            project.members.delete(assignee.id)


    def create_projects(self):
        ''' Create the exercise project under the subgroups.
        If the subgroup list is empty, create project under the master group.

        '''

        # We do not want to clone the templates multiple times.
        cloned=[]
        for subgrp in self.subgroups:
            for ex in self.exercises:
                # This is the name of the project.
                project=ex.project
                # Create project only if it does not exist
                if not list(filter(lambda x: x.name == project, subgrp.projects.list(get_all=True))):
                    try:
                        print('Creating project %s under %s' %(project, subgrp.name))
                        proj = self.gl.projects.create({
                        'name' : ex.project_name,
                        'description' : ex.project_description,
                        'namespace_id' : subgrp.id
                        })
                        if ex.project_template:
                            # Figure out the name of the directory
                            filters=[ r'.*:', r'.*/', r'\.git' ]
                            template_dir=ex.project_template
                            for filt in filters:
                                template_dir = re.sub(filt, '', template_dir)

                            # Clone if not already cloned
                            if template_dir not in cloned:
                                subprocess.check_output(
                                        "rm -rf %s && git clone %s"
                                        %(template_dir,ex.project_template), shell = True )
                                cloned.append(template_dir)
                            target_url =  proj.ssh_url_to_repo
                            subprocess.check_output(
                                    "cd ./%s  && git checkout %s && git remote add new %s && git push new %s:%s"
                                    %(template_dir,
                                      ex.project_template_branch,target_url,
                                      ex.project_template_branch, ex.project_target_branch
                                      ), shell = True )
                            subprocess.check_output("sync ./%s" %(template_dir),
                                                    shell = True)
                    except GitlabCreateError:
                        # We should not end here
                        print('Project %s under %s already exists' %(project, subgrp.name))

        for dir in cloned:
            subprocess.check_output("rm -rf ./%s" %(dir),
                                    shell = True)

    def delete_projects(self):
        ''' Delete exercise project under the subgroups.
        If the subgroup list is empty, delete project under the master group.

        '''
        # This is an indicator if something is deleted
        deleted = False
        for subgrp in self.subgroups:
            undeleted=[]
            for ex in self.exercises:
                # This is the name of the project.
                project=ex.project
                project_obj=list(filter(lambda x: x.name == project, subgrp.projects.list()))
                if project_obj:
                    project_obj=project_obj[0]
                    try:
                        answer=False
                        while answer != 'n' and answer != 'y' and ( project_obj not in undeleted):
                            print('Deleting project %s under %s' %(project_obj.name, subgrp.name))
                            print('All project data will be lost.')
                            answer=input('Is this what you want [n/y]')
                            if answer == 'y':
                                self.gl.projects.delete(project_obj.id)
                                deleted = True
                                print('Project %s deleted' %(project_obj.name))
                            else:
                                print('Project %s not deleted' %(project_obj.name))
                                undeleted.append(project_obj)
                    except:
                        print('Project %s does not exist' %(project))
        # Something deleted, need to let the server to stabilize
        if deleted:
            waittime=10
            print('Waiting %s seconds for server to stabilize' %(waittime))
            time.sleep(waittime)
    ### Project handling ends ###

    ### Exercise handling ###
    @property
    def exercises(self):
        if not hasattr(self, '_exercises'):
            self._exercises=[]
        return self._exercises

    def add_exercise(self,**kwargs):
        ''' Creates an exercise issue with title, due date and contents defined in a file
        to a given project.

        Arguments
        ---------
        due_date : str
        file : str
        project : str
        '''
        due_date=kwargs.get('due_date')
        file=kwargs.get('file')
        project=kwargs.get('project')
        project_template=kwargs.get('project_template',None)
        project_template_branch=kwargs.get('project_template_branch','master')
        project_target_branch=kwargs.get('project_target_branch','master')
        project_description=kwargs.get('project_description')
        substitution_list=kwargs.get('substitution_list', [])

        self.exercises.append(exercise(project=project,
                                       due_date=due_date,
                                       file=file,
                                       project_template=project_template,
                                       project_template_branch=project_template_branch,
                                       project_target_branch=project_target_branch,
                                       project_description=project_description,
                                       substitution_list=substitution_list
                                       ))


    def create_exercises(self):
        '''For all assignees:
        * Create projects for the exercises
        * Create exercise issues for all assignees.

        '''
        # Add assgnees to subgroups
        #Create project to subgroups
        self.create_projects()
        # If there is a subgroup per assignee, create exercises to projects in subgroups
        if len(self.exercise_subgroups) > 0 and (len(self.assignee_ids) == len(self.subgroups)):
            self.assignees_to_subgroups()
            for sg,assignee_id in list(zip(self.subgroups,self.assignee_ids)):
                for exercise in self.exercises:
                    # Check for existing project and give access
                    proj=self.gl.projects.get(sg.projects.list(search=exercise.project,get_all=True)[0].id)
                    # Test for existing user user need access rights anyway.
                    self.set_project_access_level(proj=proj,assignee_id=assignee_id)
                    print('Creating exercise %s' %(exercise.title))
                    exercise.create_issue(proj=proj, assignee_id=assignee_id)
        elif len(self.exercise_subgroups)==0:
            sg = self.mastergroup #set to mastergroup
            for exercise in self.exercises:
                proj=self.gl.projects.get(sg.projects.list(search=exercise.project,get_all=True)[0].id)
                if len(self.assignee_ids) >0:
                    for assignee_id in self.assignee_ids:
                    # If we wish to assign exercise for every user, there must be a exercise for every user.
                        # Create axercise
                        print('Creating exercise %s' %(exercise.title))
                        if self.assign:
                            # give access
                            self.set_project_access_level(proj=proj,assignee_id=assignee_id)
                            exercise.create_issue(proj=proj, assignee_id=assignee_id)
                        # We do not assign
                        else:
                            exercise.create_issue(proj=proj)
                else:
                    print('No assignees given, creating exercise without assignee.')
                    exercise.create_issue(proj=proj)
        else:
            print("ERROR: Assignee-project count discrepancy")


    def check_exercises(self):
        '''For all assignees:
        * From the projects for the exercises
        * Check the exercise issues status for all assignees.

        '''

        # If there is a subgroup per assignee, check  exercises in that group
        if len(self.exercise_subgroups) > 0 and (len(self.assignee_ids) == len(self.subgroups)):
            for sg,assignee_id in list(zip(self.subgroups,self.assignee_ids)):
                for exercise in self.exercises:
                    if len(sg.projects.list()) > 0:
                        proj=self.gl.projects.get(sg.projects.list(search=exercise.project,get_all=True)[0].id)
                        #Update existing issue in proj
                        existing_issue=list(filter(lambda x: x.title == exercise.title, proj.issues.list()))
                        if existing_issue:
                            existing_issue=existing_issue[0]
                            print('Issue with title %s found in project %s'
                                    %(exercise.title,proj.name))
                            print('State of the issue is %s' %(existing_issue.state))
                            exercise.assigned[self.gl.users.get(assignee_id).name] = (existing_issue.id,existing_issue.state,proj,self.gl.users.get(assignee_id))
                    else:
                            print('No projects found for in group %s' %(sg.name))
        # If create all exercises to project
        elif len(self.exercise_subgroups)==0:
            for sg in self.subgroups: #set to mastergroup
                for assignee_id in self.assignee_ids:
                    for exercise in self.exercises:
                        # Check for existing titles
                        proj=self.gl.projects.get(sg.projects.list(search=exercise.project)[0].id)
                        #try:
                        #    member=proj.members.create({'user_id':assignee_id,'access_level': gitlab.const.AccessLevel.MAINTAINER})
                        #except GitlabCreateError:
                        #    member = proj.members.delete(assignee_id)
                        #    member=proj.members.create({'user_id':assignee_id,'access_level': gitlab.const.AccessLevel.MAINTAINER})
                        #existing_issue=proj.issues.list(search=exercise.title)
                        existing_issue=filter(lambda x: x.title == exercise.title, proj.issues.list())[0]
                        if existing_issue:
                            print('Issue with title %s found in project %s'
                                %(exercise.title,proj.name))
                            print('State of the issue is %s' %(existing_issue.state))
                            exercise.assigned[self.gl.users.get(assignee_id).name] = (existing_issue.id,existing_issue.state)
                        else:
                            print('Issue not found')

        else:
            print("ERROR: Assignee-project count discrepancy")

    ### Exercise handling end ###

    ### User identification   ###
    def find_users(self,**kwargs):
        '''Finds Gitlab user objects based on given list of search strings.

        Arguments
        ---------
        find : [ str ]
            List of search strings.
        '''
        users=[]
        undefined=[]
        find=kwargs.get('find',None)
        for string in find:
            user=self.gl.users.list(username=string)
            if not user:
                user=self.gl.users.list(search=string)
            if len(user) > 1:
                found=[]
                print('WARNING: Unambiquous search result for string %s. Trying automated exact match.' %(string))
                for usr in user:
                    namelist=usr.name.split()
                    stringlist=string.split()
                    unmatched=[x for x in stringlist if x not in namelist]
                    if len(unmatched) == 0:
                        found.append(usr)
                if len(found) == 0:
                    print('User %s not found'%(string))
                elif len(found) > 1:
                    sys.exit('ERROR: Unambiquous search result for string %s. Please refine.' %(string))
                elif len(found) == 1:
                    print('Automated match with \'%s\' and \'%s\'' %(string,found[0].name))
                    print('Using %s' %(found[0].name))
                    users.append(found[0])
                else:
                    sys.exit('ERROR: We should not end here in search for string %s. Please refine.' %(string))
            elif len(user) < 1:
                print('User %s not found'%(string))
                undefined.append(string)
            else:
                # Append the only found user
                users.append(user[0])
        return users,undefined

    def get_user_parameters(self,**kwargs):
        ''' Extract list of field values from a list of GitlabUser objects

        Parameters
        ----------
            field : str
                field to extract

            users : [ GitlabUsers]

        '''
        field=kwargs.get('field')
        users=kwargs.get('users')
        parameters=[ getattr(user,field) for user in users]
        return parameters

    def set_project_access_level(sel,**kwargs):
        access_level = kwargs.get('access_level', 'MAINTAINER')
        assignee_id = kwargs.get('assignee_id', None)
        proj = kwargs.get('proj', None)
        if proj == None:
            print('Project must be given')
            sys.exit
        else:
            # Test for existing user user need access rights anyway.
            if proj.members_all.list(id=assignee_id):
                member = proj.members_all.list(id=assignee_id)[0]
                if member.access_level < getattr(gitlab.const.AccessLevel,'%s' %(access_level)):
                    member.access_level  = getattr(gitlab.const.AccessLevel,'%s' %(access_level))
                    member.save()
                else:
                    print('Member access level is higher or equal than %s'
                            %(getattr(gitlab.const.AccessLevel,'%s' %(access_level))))
            else:
                # Not a member, grant access.
                member=proj.members.create({'user_id':assignee_id,'access_level':
                    getattr(gitlab.const.AccessLevel,'%s' %(access_level))
                    })


if __name__ == "__main__":
    #This is a self test, not the main interface.
    # This is an example code how to create issues to a project.
    import time
    import argparse
    import pdb
    import exercise_manager

    # Implement argument parser
    parser = argparse.ArgumentParser(description='Parse selectors')
    parser.add_argument('--url', dest='url', type=str, nargs='?', const = True,
            default=None,help='URL of the gitlab server')

    parser.add_argument('--token',   dest='token', type=str, nargs='?', const=True,
            default=None, help='Access token for gitlab server access.')

    parser.add_argument('--group',   dest='group', type=str, nargs='?', const=True,
            default=None, help='Master group for the exercise.')

    parser.add_argument('--no-ssl',   dest='ssl', type=bool, nargs='?', const=False,
            default=True, help='Set to false to disable SSL verification.')

    args=parser.parse_args()
    ex=exercise_generator(url=args.url, token=args.token, group=args.group, ssl=args.ssl)
    #ex.protected_subgroups=['Marko Kosunen']
    list_of_names=['Marko Kosunen']
    ex.exercise_subgroups=ex.get_user_parameters(
            users=ex.find_users(find=list_of_names),
            field='name')
    ex.assignee_ids=ex.get_user_parameters(
            users=ex.find_users(find=list_of_names),
            field='id')
    ex.delete_subgroups()
    ex.assignees_to_subgroups()
    ex.add_exercise( due_date='2022-11-31',
            project='Ex1-SPI-slave',
            file='/home/pt/mak/opetus/ELEC-E9545/gitlab_api/exercise1.md')
    ex.add_exercise( due_date='2022-11-31',
            project='Ex2-ACore',
            file='/home/pt/mak/opetus/ELEC-E9545/gitlab_api/exercise2.md')
    ex.add_exercise( due_date='2022-11-31',
            project='Ex3-PIC',
            file='/home/pt/mak/opetus/ELEC-E9545/gitlab_api/exercise3.md')
    ex.create_exercises()

